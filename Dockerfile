# semgrep helpers 2020-10-02 by @k33g | on gitlab.com 
FROM alpine:3.11
#RUN apk --update add --no-cache nodejs npm
#FROM python:3.7.7-alpine3.11

LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

RUN apk --update add --no-cache nodejs npm python3

RUN python3 -m pip install semgrep

COPY analyze.js /usr/local/bin/analyze

RUN chmod +x /usr/local/bin/analyze

CMD ["/bin/sh"]


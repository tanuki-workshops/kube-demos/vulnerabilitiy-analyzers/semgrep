#!/usr/bin/env node

console.log("👮‍♀️ semgrep analysis")

const fs = require("fs")
const { exec } = require("child_process")

let cmdArgs = process.argv.slice(2);
let directory = cmdArgs[0]

let start = new Date()

//let severity = "Critical"
let severities = {
  "WARNING": "High"
}
// Critical, High, Medium
let confidence = "Unknown"
// Unknown Confirmed Experimental

let scannerName = "SemGrep"
let scannerId = "semgrep"
let scannerSrc = "https://bots.garden"
let scannerVendor = "Bots.Garden"
let scannerVersion = "0.0.1"

// example semgrep -f https://semgrep.dev/p/r2c-security-audit *.js --json
let cmd = `semgrep -f https://semgrep.dev/p/r2c-security-audit ${directory} --json > semgrep.json`


exec(cmd, (error, stdout, stderr) => {
    
    if (stderr) {
        //console.log(`😡 vulnerabilities detected: ${stderr}`)
        
        let semgrepReport = fs.readFileSync("./semgrep.json", "utf8")
        let semgrepResults = JSON.parse(semgrepReport)
        
        //console.log(JSON.stringify(semgrepResults, null, 2))

        // ========== Helpers ==========
        let btoa = (string) => Buffer.from(string).toString("base64")

        let get_cve_id = (description, path, line) => `${btoa(description)}-${btoa(path)}-${btoa(line)}`

        let get_dependency = () => { 
          return { 
            package: {} 
          } 
        }   
             
        let get_scanner = () => {
          return {
            id: scannerId, name: scannerName
          }
        }
        // ========== End of Helpers ==========

        //console.log("📝 semgrep report:")
        //console.log(`------------------------------------------------------------`)
        semgrepResults.results.forEach(item => {
          console.log(`------------------------------------------------------------`)
          console.log(` 🔴 Rule:     ${item.check_id}`)
          console.log(`    Message:  ${item.extra.message}`)
          console.log(`    file:     ${item.path}`)
          console.log(`    line:     ${item.start.line}`)
          //console.log(`------------------------------------------------------------`)
        })

      
        /* --- generate gl-sast-report.json --- */
        // ========== Generate SAST report ==========
        let vulnerabilities = semgrepResults.results  
              .map(item => {
              return {
                category: "sast",
                name: `🔴: ${item.check_id}`,
                message: `📝: ${item.extra.message}`,
                description: `🖐️: ${item.extra.metadata.cwe}`,
                cve: get_cve_id(item.extra.metadata.cwe, item.path, ""+item.start.line),
                severity: severities[item.extra.severity],
                confidence: confidence,
                scanner: get_scanner(),
                location: {
                  file: item.path, 
                  start_line: item.start.line,
                  end_line: item.end.line,
                  class: item.extra.metadata.owasp,
                  method: "semgrep",
                  dependency: get_dependency()
                },
                identifiers: [
                  {
                    type: "semgrep_rule_id",
                    name: `SemGrep rule ${item.extra.metadata.cwe}`,
                    value: item.extra.metadata.owasp
                  }
                ]                       
              } // return
            })

        let end = new Date() - start

        let sastReport = {
          version: "3.0",
          vulnerabilities: vulnerabilities,
          remediations: [],
          scan: {
            scanner: {
              id: scannerId,
              name: scannerName,
              url: scannerSrc,
              vendor: {
                name: scannerVendor
              },
              version: scannerVersion
            },
            type: "sast",
            start_time: start,
            end_time: end,
            status: "success" // TODO try catch
          }
        }

        fs.writeFileSync("./gl-sast-report.json", JSON.stringify(sastReport, null, 2))
        return
    } else {
        fs.writeFileSync("./gl-sast-report.json", "[]")
    }

    if (error) {
        console.log(`error: ${error}`)
        return
    }
    console.log(`stdout: ${stdout}`)
})

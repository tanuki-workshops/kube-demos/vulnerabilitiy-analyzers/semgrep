# SemGrep SAST GitLab Analyzer

## Use it

```yaml
stages:
  - 🔎code-quality

include:
  - project: 'tanuki-workshops/kube-demos/vulnerabilitiy-analyzers/semgrep'
    file: 'semgrep.gitlab-ci.yml'

👮‍♀️:vulnerability:detection:
  stage: 🔎secure
  extends: .semgrep:analyzer  
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  script: |
    analyze ./
```

## SemGrep report sample

```json
let sample_do_not_use = {
  "results": [
    {
      "check_id": "javascript.lang.security.detect-eval-with-expression.detect-eval-with-expression",
      "path": "main.js",
      "start": {
        "line": 16,
        "col": 3
      },
      "end": {
        "line": 16,
        "col": 15
      },
      "extra": {
        "message": "Detected eval(variable), which could allow a malicious actor to run arbitrary code.\n",
        "metavars": {
          "$OBJ": {
            "start": {
              "line": 16,
              "col": 8,
              "offset": 254
            },
            "end": {
              "line": 16,
              "col": 14,
              "offset": 260
            },
            "abstract_content": "params",
            "unique_id": {
              "type": "id",
              "value": "params",
              "kind": "Param",
              "sid": 5
            }
          }
        },
        "metadata": {
          "cwe": "CWE-95: Improper Neutralization of Directives in Dynamically Evaluated Code ('Eval Injection')",
          "owasp": "A1: Injection",
          "source-rule-url": "https://github.com/nodesecurity/eslint-plugin-security/blob/master/rules/detect-eval-with-expression.js"
        },
        "severity": "WARNING",
        "lines": "  eval(params)"
      }
    }
  ],
  "errors": []
}
```
